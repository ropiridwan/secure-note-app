import React, {useState} from 'react';
import {
  View,
  TextInput,
  Alert,
  StyleSheet,
  TouchableOpacity,
  Text,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CryptoJS from 'react-native-crypto-js';

const AddNoteScreen = ({navigation, route}) => {
  const {itemNote, edit} = route.params;
  const [note, setNote] = useState(itemNote);

  const encriptData = item => {
    try {
      const encryptionKey = 'secret key 123';
      if (edit) {
        const encryptedNote = CryptoJS.AES.encrypt(
          item,
          encryptionKey,
        ).toString();
        return encryptedNote;
      } else {
        const encryptedNote = CryptoJS.AES.encrypt(
          note,
          encryptionKey,
        ).toString();
        return encryptedNote;
        // return setEncriptNote(encryptedNote);
      }
    } catch (error) {
      Alert.alert('Trouble when encrypt note');
    }
  };

  const editData = async () => {
    const data = await AsyncStorage.getItem('encryptedNote');
    const savedNote = JSON.parse(data);
    const noteDecrypt = [];
    savedNote.map(item => {
      let bytes = CryptoJS.AES.decrypt(item.note, 'secret key 123');
      let originalText = bytes.toString(CryptoJS.enc.Utf8);
      return noteDecrypt.push({
        note: originalText,
      });
    });

    const replacedArray = noteDecrypt.map(item => {
      return item.note.replace(itemNote, note);
    });

    const dataPush = [];
    replacedArray.map(item => {
      const originalText = encriptData(item);
      dataPush.push({
        note: originalText,
      });
    });

    const jsonValue = JSON.stringify(dataPush);
    await AsyncStorage.setItem('encryptedNote', jsonValue);

    Alert.alert('Note Saved');
    navigation.goBack();
  };

  const handleSaveNote = async () => {
    if (edit) {
      editData();
    } else {
      let data = [];
      const getItem = await AsyncStorage.getItem('encryptedNote');
      if (getItem == null) {
        const note = encriptData();
        data = [
          {
            note,
          },
        ];
      } else {
        data = JSON.parse(getItem);
        if (data.some(item => item.note == note.note)) {
          Alert.alert('Please write different note, it is existing');
        } else {
          const note = encriptData();
          data.unshift({
            note,
          });
        }
      }
      const jsonValue = JSON.stringify(data);
      await AsyncStorage.setItem('encryptedNote', jsonValue);

      Alert.alert('Note Saved');
      navigation.goBack();
    }
  };

  return (
    <View style={styles.header}>
      <TextInput
        style={styles.textInput}
        multiline
        placeholder="write some note..."
        value={note}
        onChangeText={setNote}
      />
      <TouchableOpacity style={styles.button} onPress={() => handleSaveNote()}>
        <Text style={{color: '#fff', alignSelf: 'center'}}>
          {edit ? 'Save' : 'Add New'}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default AddNoteScreen;

const styles = StyleSheet.create({
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  button: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: 'black',
    borderRadius: 6,
    marginVertical: 12,
  },
  textInput: {
    borderColor: 'black',
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 12,
    paddingHorizontal: 12,
    marginHorizontal: 18,
  },
  detailModal: {
    backgroundColor: 'white',
    paddingHorizontal: 12,
    paddingVertical: 20,
    justifyContent: 'center',
    borderRadius: 25,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
