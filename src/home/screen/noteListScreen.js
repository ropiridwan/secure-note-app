import React, {useCallback, useEffect, useState} from 'react';
import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFocusEffect} from '@react-navigation/native';
import CryptoJS from 'react-native-crypto-js';

const NoteListScreen = ({navigation}) => {
  const [notes, setNotes] = useState([]);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);

  function handleBackButtonClick() {
    return true;
  }

  async function fetchData() {
    try {
      const data = await AsyncStorage.getItem('encryptedNote');
      const savedNote = JSON.parse(data);
      if (savedNote !== null) {
        const updatedNotes = savedNote.map(item => {
          let bytes = CryptoJS.AES.decrypt(item.note, 'secret key 123');
          let originalText = bytes.toString(CryptoJS.enc.Utf8);
          return {
            note: originalText,
          };
        });

        setNotes(updatedNotes);
      } else {
        Alert.alert('Please add some note first');
      }
    } catch (error) {
      Alert.alert('Error fetching data');
    }
  }

  useFocusEffect(
    useCallback(() => {
      fetchData();
    }, []),
  );

  return (
    <View style={styles.header}>
      {notes.map((item, index) => (
        <TouchableOpacity
          key={index}
          style={styles.containerItem}
          onPress={() =>
            navigation.navigate('addNoteScreen', {
              itemNote: item.note,
              edit: true,
            })
          }>
          <Text style={{color: 'black', fontSize: 14, fontWeight: '700'}}>
            {index + 1}
            {'. '}
          </Text>
          <Text style={{color: 'black', fontSize: 14, fontWeight: '700'}}>
            {item?.note}
          </Text>
        </TouchableOpacity>
      ))}

      <TouchableOpacity
        style={styles.button}
        onPress={() =>
          navigation.navigate('addNoteScreen', {itemNote: '', edit: false})
        }>
        <Text style={{color: '#fff', alignSelf: 'center'}}>Add New Note</Text>
      </TouchableOpacity>
    </View>
  );
};

export default NoteListScreen;

const styles = StyleSheet.create({
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  containerItem: {
    width: '80%',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 12,
    paddingVertical: 8,
    paddingHorizontal: 12,
    marginBottom: 12,
    flexDirection: 'row',
  },
  button: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: 'black',
    borderRadius: 6,
    marginVertical: 12,
  },
});
