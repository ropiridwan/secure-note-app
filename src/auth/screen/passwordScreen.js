import React, {useState} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Text,
} from 'react-native';
import Keychain from 'react-native-keychain';

const PasswordConfigScreen = ({navigation}) => {
  const [password, setPassword] = useState('');

  const handleSetPassword = async () => {
    await Keychain.setGenericPassword('user', password, {
      accessControl: Keychain.ACCESS_CONTROL.BIOMETRY_ANY,
      accessible: Keychain.ACCESSIBLE.WHEN_UNLOCKED,
    });
    navigation.goBack();
  };

  return (
    <View style={styles.header}>
      <TextInput
        style={styles.textInput}
        placeholder="Enter password"
        secureTextEntry
        value={password}
        onChangeText={setPassword}
      />
      <TouchableOpacity style={styles.button} onPress={handleSetPassword}>
        <Text style={{color: '#fff'}}>Save Password</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PasswordConfigScreen;

const styles = StyleSheet.create({
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  textInput: {
    borderColor: 'black',
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 12,
    paddingHorizontal: 12,
  },
  button: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: 'black',
    borderRadius: 6,
  },
});
