import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Alert,
} from 'react-native';
import Keychain from 'react-native-keychain';
import Modal from 'react-native-modal';

const authenticationScreen = ({navigation}) => {
  const [modalPassword, setModalPassword] = useState(false);
  const [passwordd, setPasswordd] = useState('');

  const configureBiometrics = async () => {
    try {
      const isSupported = await Keychain.getSupportedBiometryType();
      const credentials = await Keychain.getGenericPassword();
      if (isSupported) {
        if (credentials) {
          navigation.navigate('noteListScreen');
        } else {
          navigation.navigate('passwordScreen');
          Alert.alert(
            'Failed to configure authentication biometrics, please set password first',
          );
        }
      } else {
        Alert.alert('Device not support for authentication biometrics');
      }
    } catch (error) {
      const err = Object.assign({}, error);
      var properties = err.message.split(', ');
      var obj = {};
      properties.forEach(function (property) {
        var tup = property.split(':');
        obj[tup[0]] = tup[1];
      });
      if (
        obj.msg == ' Terlalu sering dicoba. Sensor sidik jari dinonaktifkan.' ||
        obj.msg == ' Terlalu banyak upaya. Coba lagi nanti.'
      ) {
        setModalPassword(true);
      }
    }
  };

  const handleSetPassword = async () => {
    try {
      const credentials = await Keychain.getGenericPassword();
      if (credentials) {
        const {password} = credentials;
        if (password == passwordd) {
          navigation.navigate('noteListScreen');
        } else {
          await modalPassword(false);
          Alert.alert('Wrong Password!');
        }
      } else {
        Alert.alert('No password found in the keychain.');
      }
    } catch (error) {
      const err = Object.assign({}, error);
      var properties = err.message.split(', ');
      var obj = {};
      properties.forEach(function (property) {
        var tup = property.split(':');
        obj[tup[0]] = tup[1];
      });
      if (
        obj.msg == ' Terlalu sering dicoba. Sensor sidik jari dinonaktifkan.' ||
        obj.msg == ' Terlalu banyak upaya. Coba lagi nanti.'
      ) {
        navigation.navigate('noteListScreen');
      }
    }
  };

  return (
    <View style={styles.header}>
      <Text
        style={{
          paddingBottom: 16,
          color: 'black',
          fontSize: 20,
          fontWeight: '700',
        }}>
        Configuration Authentication Biometrics
      </Text>
      <TouchableOpacity
        style={styles.button}
        onPress={() => configureBiometrics()}>
        <Text style={{color: 'white'}}>Configuration Biometrics</Text>
      </TouchableOpacity>
      {modalPassword && (
        <Modal
          statusBarTranslucent
          isVisible={modalPassword}
          onBackdropPress={() => {
            setModalPassword(false), setPasswordd('');
          }}
          animationType="slide">
          <View style={styles.detailModal}>
            <TextInput
              style={styles.textInput}
              placeholder="Masukkan kata sandi"
              secureTextEntry
              value={passwordd}
              onChangeText={setPasswordd}
            />
            <TouchableOpacity
              style={styles.button}
              onPress={() => handleSetPassword()}>
              <Text style={{color: '#fff', alignSelf: 'center'}}>
                Lanjutkan
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      )}
    </View>
  );
};

export default authenticationScreen;

const styles = StyleSheet.create({
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  button: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: 'black',
    borderRadius: 6,
    marginVertical: 12,
  },
  textInput: {
    borderColor: 'black',
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 12,
    paddingHorizontal: 12,
  },
  detailModal: {
    backgroundColor: 'white',
    paddingHorizontal: 12,
    paddingVertical: 20,
    justifyContent: 'center',
    borderRadius: 25,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
