import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

const welcomeScreen = ({navigation}) => {
  return (
    <View style={styles.header}>
      <Text style={styles.text}>Welcome!</Text>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('authScreen')}>
        <Text style={{color: 'white'}}>Start</Text>
      </TouchableOpacity>
    </View>
  );
};

export default welcomeScreen;

const styles = StyleSheet.create({
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  text: {color: 'black', fontWeight: '700', fontSize: 20},
  button: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: 'black',
    borderRadius: 6,
    marginVertical: 12,
  },
});
