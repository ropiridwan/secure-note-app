import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import authenticationScreen from './src/auth/screen/authenticationScreen';
import welcomeScreen from './src/auth/screen/welcomeScreen';
import PasswordConfigScreen from './src/auth/screen/passwordScreen';
import NoteListScreen from './src/home/screen/noteListScreen';
import AddNoteScreen from './src/home/screen/addNoteScreen';

const Stack = createNativeStackNavigator();

const App = ({navigation}) => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="welcomeScreen">
        <Stack.Screen
          name="welcomeScreen"
          component={welcomeScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="authScreen"
          component={authenticationScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="passwordScreen"
          component={PasswordConfigScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="noteListScreen"
          component={NoteListScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="addNoteScreen"
          component={AddNoteScreen}
          options={{
            // headerShown: false,
            title: 'Add/Edit Note',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
